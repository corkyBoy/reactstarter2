import './index.scss'
import json from './index.json'
import * as React from 'react'
import * as ReactDom from 'react-dom'

const App = () => {
	return (
		<div>
			<p id="message">HELLO THERE!!!</p>
			<pre>{JSON.stringify(json, null, 4)}</pre>
		</div>
	)
}

ReactDom.render(<App />, document.getElementById('app'))