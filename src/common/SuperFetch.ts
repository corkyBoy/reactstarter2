export class SuperFetch {
	constructor() {

	}

	async get<TResponse>(url: string): Promise<TResponse> {
		const response = await fetch(url, {
			method: 'GET'
		})

		if (response.ok) {
			return this.tryHandleJsonResponse(response)
		}

		this.handleError(response)
	}

	async post<TResponse, TBody>(url: string, body: TBody): Promise<TResponse> {
		const response = await fetch(url, {
			method: 'POST',
			headers: {
				'Content-Type': 'application/json'
			},
			body: JSON.stringify(body)
		})

		if (response.ok) {
			return this.tryHandleJsonResponse(response)
		}

		this.handleError(response)
	}

	async put<TResponse, TBody>(url: string, body: TBody): Promise<TResponse> {
		const response = await fetch(url, {
			method: 'PUT',
			headers: {
				'Content-Type': 'application/json'
			},
			body: JSON.stringify(body)
		})

		if (response.ok) {
			return this.tryHandleJsonResponse(response)
		}

		this.handleError(response)
	}

	async delete(url: string): Promise<void> {
		const response = await fetch(url, {
			method: 'DELETE'
		})

		if (response.ok) {
			return
		}

		this.handleError(response)
	}

	private async tryHandleJsonResponse<TResponse>(response: Response): Promise<TResponse | undefined> {
		if (response.headers.get('content-type')?.includes('application/json')) {
			return response.json()
		}

		return undefined
	}

	private handleError(response: Response): never {
		throw new Error(`${response.statusText} ${response.status}`)
	}
}